SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Function dbo.ICE_IsCountryUSCanada(@Country varchar(200))
returns tinyint
as
Begin
if @Country Like 'USA' or @Country Like 'UNITED STATES' or @Country Like 'CANADA'
	return 1
return 0
End
GO
