SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Function dbo.ICE_GetCDCharge(@CD tinyint)
returns money
as
Begin
declare @CDCharge money
if ISNULL(@CD, 0) = 0
	set @CDCharge = NULL
else
	set @CDCharge = (Select CDCharge from ICE_Configurations where ID=1)
return @CDCharge
End
GO
