SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[ICE_GetGreater](@a sql_variant, @b sql_variant)
returns sql_variant
as
Begin
if @a > @b
	return @a

return @b
End
GO
