SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
/* =============================================
 Author:		Pankaj Sutradhar
 Create date: 08/28/2007
 Description:	Get template Details
 =============================================*/
CREATE VIEW [dbo].[uv_OTL_TemplateDetails]
AS
SELECT     T.Id, T.UserId, T.ProjectTypeId, T.DatabaseTypeId, T.OutputTypeId, T.ApplicationTypeId, T.Title, T.ShortDesc, T.LongDesc, T.Copyright, 
                      T.ScriptLanguage, T.OutputLanguage, T.DownloadCount, T.ViewCount, T.LinesCount, T.Status, CAST(FLOOR(CAST(T.DateUpdated AS float)) AS datetime) 
                      AS DateUpdated, CAST(FLOOR(CAST(T.DateLoaded AS float)) AS datetime) AS DateLoaded, T.DiscussionLink, dbo.tblAuthor.Username AS Poster, 
                      AppT.Name AS ApplicationName, dbo.OTL_OutputTypes.Name AS CategoryName, T.DateUpdated AS LastModified, T.TemplateFileName, 
                      T.ArchiveFileName
FROM         dbo.tblAuthor INNER JOIN
                      dbo.OTL_Templates AS T ON dbo.tblAuthor.Author_ID = T.UserId INNER JOIN
                      dbo.OTL_ApplicationTypes AS AppT ON T.ApplicationTypeId = AppT.Id INNER JOIN
                      dbo.OTL_DatabaseTypes ON T.DatabaseTypeId = dbo.OTL_DatabaseTypes.Id INNER JOIN
                      dbo.OTL_OutputTypes ON T.OutputTypeId = dbo.OTL_OutputTypes.Id
GO
