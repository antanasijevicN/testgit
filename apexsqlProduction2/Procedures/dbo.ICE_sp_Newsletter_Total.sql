SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE dbo.ICE_sp_Newsletter_Total
AS
BEGIN
      SELECT
          count(*) as Total
      FROM
          apexsql.Newsletter
      WHERE
          apexsql.Newsletter.Status = 1
END	
GO
