SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[wwfSpForumEmailNotify] 
(
@lngAuthorID int, 
@intForumID int 
)
 AS 
SELECT tblEmailNotify.* 
FROM tblEmailNotify 
WHERE tblEmailNotify.Author_ID = @lngAuthorID AND tblEmailNotify.Forum_ID = @intForumID;
GO
