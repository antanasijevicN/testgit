SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE prc_Newsletter_ins

@UserEmail varchar(50),
@Status bit

As	
	INSERT
	INTO [Newsletter]
	(
[UserEmail],[Status]
	)
	VALUES
	(
@UserEmail,@Status
	)

	RETURN  @@Error
GO
