SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE dbo.ICE_sp_ApexEmployeesAccounts
AS
BEGIN
SELECT 	[Author_ID],	
	[Username]	
FROM
	[apexsql].[dbo].[tblAuthor]
WHERE dbo.tblAuthor.Active = 'True'
AND dbo.tblAuthor.Group_ID not in (4,2,5,6,20)
END
GO
