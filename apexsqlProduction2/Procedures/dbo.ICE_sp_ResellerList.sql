SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[ICE_sp_ResellerList]
as
select Reseller_ID, Company+' - '+First_Name+' '+Last_Name+' - '+convert(varchar(20), Discount)+'%' as Reseller from ICE_Resellers order by 2
GO
