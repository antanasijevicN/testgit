SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER OFF
GO

CREATE  PROCEDURE dbo.up_Cats_Select_All
AS
BEGIN
 
 SELECT CatID, CatName, Colour, Age
  FROM blockwood.Cats
 
END
GO
