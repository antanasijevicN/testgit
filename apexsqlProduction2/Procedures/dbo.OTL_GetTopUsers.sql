SET ANSI_NULLS OFF
SET QUOTED_IDENTIFIER OFF
GO


-- =============================================
-- Author:	Pankaj Sutradhar
-- Create date:09/ 04/2007
-- Description:	get top 4 users
-- =============================================
CREATE PROCEDURE [dbo].[OTL_GetTopUsers]
 AS
	BEGIN
		SELECT TOP 4* FROM uv_OTL_BrowseByMember
	END
GO
