SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE get_resellers
AS
SELECT DISTINCT
	id=reseller,
	name=CAST(reseller AS varchar(100))
FROM
	reference
GO
