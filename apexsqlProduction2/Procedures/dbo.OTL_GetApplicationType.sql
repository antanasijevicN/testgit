SET ANSI_NULLS OFF
SET QUOTED_IDENTIFIER OFF
GO


-- =============================================
-- Author:	Pankaj Sutradhar
-- Create date: 09/04/2007
-- Description:	Get applicationtype
-- =============================================
CREATE PROCEDURE [dbo].[OTL_GetApplicationType]
 AS
	BEGIN
		SELECT * FROM uv_OTL_BrowseByApplicationType
	END
GO
