SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE    [dbo].[VIP_TopVipsByRange]
(
	@StartDate datetime,
	@EndDate datetime
)
AS
BEGIN
	SET @EndDate = @EndDate + ' 23:59:59'
	select top 30 *
	from dbo.VIP_fn_TopVips(@StartDate,@EndDate)
END

GO
