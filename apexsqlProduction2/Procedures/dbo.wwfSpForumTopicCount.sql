SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[wwfSpForumTopicCount] 
(
@intForumID int
)
 AS 
SELECT Count(tblTopic.Forum_ID) AS Topic_Count From tblTopic WHERE tblTopic.Forum_ID = @intForumID
GO
