SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[ICE_sp_ListParams](@reportid int)
as
select paramname, datatype from asprpt_tbl_reportparams where paramreportid=@reportid
GO
