SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Roles] (
		[RoleID]          [uniqueidentifier] NOT NULL,
		[RoleName]        [nvarchar](260) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Description]     [nvarchar](512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[TaskMask]        [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[RoleFlags]       [tinyint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Roles]
	ADD
	CONSTRAINT [PK_Roles]
	PRIMARY KEY
	NONCLUSTERED
	([RoleID])
	ON [PRIMARY]
GO
CREATE UNIQUE CLUSTERED INDEX [IX_Roles]
	ON [dbo].[Roles] ([RoleName])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[Roles] SET (LOCK_ESCALATION = TABLE)
GO
