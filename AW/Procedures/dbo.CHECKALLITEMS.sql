SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CHECKALLITEMS]
AS


SET NOCOUNT ON;
        DECLARE @T VARCHAR(MAX)
              DECLARE @TARGETDLGHANDLE UNIQUEIDENTIFIER
              DECLARE TEST_CURSOR CURSOR
                           FOR SELECT  CAST(ISNULL(i_itemc, ISNULL(I_ITEMP,ISNULL(I_ITEMG,ISNULL(I_ITEMV,AHZTTX)))) AS VARCHAR(MAX)) AS T
FROM DCCOMBINED.ICI 
LEFT JOIN DCCOMBINED.dc ON ici.DC = dc.dc_Id
LEFT JOIN DCCOMBINED.IGI ON I_ITEMC = I_ITEMG AND IGI.DC = ici.DC
LEFT JOIN DCCOMBINED.IVR ON I_ITEMC = I_ITEMV AND IVR.DC = ici.DC
LEFT JOIN DCCOMBINED.IPS ON I_ITEMC = I_ITEMP AND IPS.DC = ici.DC
LEFT JOIN DCCOMBINED.BIAHREP 
ON I_ITEMC = AHZTTX AND BIAHREP.DC = ici.DC
WHERE ici.dc = 3 AND (I_ITEMP IS NULL OR I_ITEMG IS NULL OR I_ITEMV IS NULL OR AHZTTX IS NULL OR I_ITEMC IS NULL)

    DECLARE @SB UNIQUEIDENTIFIER
    select @SB= service_broker_guid from master.sys.databases where name = DB_NAME()
    
              OPEN TEST_CURSOR
              FETCH NEXT FROM TEST_CURSOR INTO @T
              WHILE @@FETCH_STATUS = 0
              BEGIN
                     SET @TARGETDLGHANDLE = NEWID()
                     BEGIN DIALOG CONVERSATION @TargetDlgHandle
                                  FROM SERVICE [//BOPS/CHECKITEM]
                                  TO SERVICE '//BOPS/CHECKITEM', @SB
                                  ON CONTRACT [//EDB/InternalContract]
                                  WITH ENCRYPTION = OFF;
                                  SEND ON CONVERSATION @TargetDlgHandle
                                  MESSAGE TYPE [//EDB/InternalRequest] (@T);
                     --END
              FETCH NEXT FROM TEST_CURSOR INTO @T
              END
              CLOSE TEST_CURSOR
              DEALLOCATE TEST_CURSOR

GO
