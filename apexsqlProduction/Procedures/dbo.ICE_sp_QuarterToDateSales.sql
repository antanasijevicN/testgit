SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE     [dbo].[ICE_sp_QuarterToDateSales]
AS
BEGIN
	DECLARE @D datetime
	SET @D = GETDATE()
	EXEC dbo.ICE_sp_QuarterSales @Today = @D
END
GO
