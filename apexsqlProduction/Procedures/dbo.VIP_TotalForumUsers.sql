SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE  dbo.VIP_TotalForumUsers
AS 
BEGIN
	SELECT Count(*) AS [Total Users] 
	FROM tblAuthor A
	WHERE (SELECT COUNT(*) FROM tblThread WHERE tblThread.Author_ID = A.Author_ID) > 0
END
GO
