SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[wwfSpGuestPoster] 
(
@lngThreadID int
)
As 
SELECT tblGuestName.Name FROM tblGuestName WHERE tblGuestName.Thread_ID = @lngThreadID;
GO
