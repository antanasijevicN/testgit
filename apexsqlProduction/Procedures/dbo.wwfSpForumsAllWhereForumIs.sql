SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[wwfSpForumsAllWhereForumIs] 
( 
@intForumID int 
) 
 AS 
SELECT tblForum.* FROM tblForum WHERE Forum_ID = @intForumID;
GO
