SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION dbo.ICE_ProductLicencedBy 
(
	-- Add the parameters for the function here
	@ProductID int
)
RETURNS VarChar(10)

AS
BEGIN
	-- Declare the return variable here
	 DECLARE @ResultVar VarChar(10)

	-- Add the T-SQL statements to compute the return value here
	select @ResultVar = case 
							when CHARINDEX (CAST(@ProductID AS varchar(400)),Bundle_Products)>0 then 'Instance' 
							else 'User' end 
	from dbo.ICE_Products 
	where Product_ID=300625332

	-- Return the result of the function
	RETURN @ResultVar

END
GO
