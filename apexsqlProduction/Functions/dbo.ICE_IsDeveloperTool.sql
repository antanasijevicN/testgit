SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[ICE_IsDeveloperTool](@Product_ID int)
returns tinyint
as
Begin
return dbo.ICE_IsProductInStudio(@Product_ID, 'D')
End
GO
