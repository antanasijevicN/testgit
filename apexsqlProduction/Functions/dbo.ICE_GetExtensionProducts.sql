SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[ICE_GetExtensionProducts](@Order_ID int)
returns varchar(500)
as
Begin
declare @Result varchar(500)set @Result = ''Declare @Product_ID int, @License_Type_ID int, @Maintenance_ID int, @Qty intDECLARE CurExtensionDetails CURSOR FOR SELECT Product_Id, Maintenance_ID, Qty FROM ICE_Extension_Details where Order_ID=@Order_ID
OPEN CurExtensionDetails
FETCH NEXT FROM CurExtensionDetails INTO @Product_ID, @Maintenance_ID, @Qty
WHILE @@FETCH_STATUS = 0
BEGIN
	set @Result = @Result + convert(varchar, @Qty) + ' ' + (Select Product_Name from ICE_Products where Product_ID=@Product_ID)
	set @Result = @Result + ' with ' + (Select Maintenance from ICE_Maintenances where Maintenance_ID=@Maintenance_ID) + ' s&u renewal,'
	FETCH NEXT FROM CurExtensionDetails INTO @Product_ID, @Maintenance_ID, @Qty
END
CLOSE CurExtensionDetails
DEALLOCATE CurExtensionDetails
return @ResultEnd
GO
