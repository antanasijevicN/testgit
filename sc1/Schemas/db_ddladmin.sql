/*=============================================================
SCRIPT HEADER

VERSION:   1.01.0002
DATE:      02-08-2017 12:44:57
SERVER:    DESKTOP-220D4F8\SQLEXPRESS

DATABASE:	aba
  Schemas:  db_ddladmin


=============================================================*/
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET NOCOUNT ON
SET XACT_ABORT ON
GO

-- BEGINNING TRANSACTION STRUCTURE
PRINT 'Beginning transaction STRUCTURE'
BEGIN TRANSACTION _STRUCTURE_
GO
-- Create Schema [db_ddladmin]
Print 'Create Schema [db_ddladmin]'
GO
ALTER AUTHORIZATION
	ON SCHEMA::[db_ddladmin]
	TO [db_ddladmin]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

-- COMMITTING TRANSACTION STRUCTURE
PRINT 'Committing transaction STRUCTURE'
IF @@TRANCOUNT>0
	COMMIT TRANSACTION _STRUCTURE_
GO

SET NOEXEC OFF
GO
